#!/bin/bash

input_hash="$1"
dict1="$2"
dict2="$3"

combinate()
{
  for i in `cat ${dict1}`; do
    for j in `cat ${dict2}`; do
      combinated="${i}${j}"
      compare_sha
    done
  done
}

compare_sha()
{
  candidate=`echo ${combinated} | sha1sum | cut -d ' ' -f 1`
  if [ ${candidate} = ${input_hash} ]; then
    echo "Password cracked! Plaintext is: ${combinated}"
    exit 0
  fi
}

combinate