#!/bin/bash

input_hash="$1"
dictionary="$2"

compare_sha()
{
  for i in `cat ${dictionary}`; do
    candidate=`echo -n $i | sha1sum | cut -d ' ' -f 1`
    if [ ${candidate} = ${input_hash} ]; then
      echo "Password cracked! Plaintext is: ${i}"
      exit 0
    fi
  done
}

compare_sha